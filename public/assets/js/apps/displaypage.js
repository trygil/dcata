var data_list = {};

function constructDataDOM(){
	var content = $('#content');
	var col, cols = [];
	var anim_sequence = [];

	// Set this to change amount of columns
	var col_count = 3;

	if(12 % col_count != 0)
		col_count = 1;

	col_class = 'col-md-' + (12 / col_count);
	
	
	col = '<div class="'+ col_class +'"></div>';

	// Create the columns
	for (var i = 0; i < col_count; i++) {
		cols.push($(col));
		content.append(cols[i]);
	};

	// Insert data to column
	for (var i = 1; i <= data_list.length; i++) {
		var thumb = $('<a style="opacity:0;width:0;" href="/view/'+ data_list[i-1].id +'" class="thumbnail col-md-12">'+
						'<img src="/photos/' + data_list[i-1].photo + '">'+
						'<div class="name classy"><h2>'+ data_list[i-1].name +'</h2></div>'+
						'</a>');
		anim_sequence.push({ elements: thumb.get(), properties: { opacity:1, width:'100%' }, options: { duration: 50 } });		
		
		if(i % col_count == 0)
			cols[col_count-1].append(thumb);
		else
			cols[((i+col_count)%col_count)-1].append(thumb);
	};

	var loaded = 0;
	$('#content img').load(function(){
		++loaded;

		if(loaded == data_list.length){
			$('.spinner:eq(0)').addClass('hide');
			content.show();
			$.Velocity.RunSequence(anim_sequence); 
		}
	});

	content.addClass('display');
}

$(document).ready(function(){
	// $('#content').hide();
	$('.spinner:eq(0)').removeClass('hide');

	$.ajax({
		url:'/list',
		dataType:'json',
		success:function(data){
			//window.screen.width
			data_list = data;
			constructDataDOM();
		}
	});
});