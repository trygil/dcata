function sidebarOut(e){
	$('#sidebar').velocity(
		{ left: $(this).width()*-1, opacity:0.5 },
		{ 
			duration: 250,
			complete: function(elements) {
				$('*:not(#sidebar)').mousemove(sidebarIn);
			}
	});
};

function sidebarIn(e){
	if(e.screenX == 0){
		$('*:not(#sidebar)').unbind('mousemove');	
		$('#sidebar').velocity({ left: 0, opacity:1 }, { duration: 250, easing: 'easeInBounce' });
	}
};

$(document).ready(function(){
	$('*:not(#sidebar)').mousemove(sidebarIn);
	$('#sidebar:not(#sidebar *)').mouseleave(sidebarOut);
});