var step = 0;
var data = {};
var signUpProcs = [
	function(){
		data.first_name = $('input#first_name').val();
		data.last_name = $('input#last_name').val();
	},
	function(){
		data.username = $('input#username').val();
	},
	function(){
		data.email = $('input#email').val();
	},
	function(){
		data.sex = $('input#sex').is(':checked');
	},
	function(){
		data.password = $('input#password').val();
	},
	function(){
		data.password_confirmation = $('input#password_confirmation').val();
	},
	function(){
		data.terms = $('input#terms').is(':checked');
	}
];

function signUpNextPrev(){
	var l = $('#register > div').length;

	if(step >= l-2){
		$('#next-btn').hide();
		$('#finish-btn').show();
	}
	else if(step <= 0){
		$('#prev-btn').hide();
		$('#finish-btn').hide();
	}else{
		$('#next-btn').show();
		$('#prev-btn').show();
		$('#finish-btn').hide();
	}

	$('#register > div.show').addClass('hide').removeClass('show');
	$('#register > div:eq('+ step +')').addClass('show').removeClass('hide');
	$('#register > div:eq('+ step +') input:eq(0)').focus();
}

$(document).ready(function(){
	signUpNextPrev();

	$('#next-btn').click(function(){
		if(signUpProcs[step] != undefined)
			signUpProcs[step]();

		step++;
		signUpNextPrev();
	});

	$('#prev-btn').click(function(){
		step--;
		signUpNextPrev();
	});

	$('#finish-btn').click(function(){
		if(signUpProcs[step] != undefined)
			signUpProcs[step]();

		$.ajax({
			url:'/account/signup',
			type:'post',
			data:window.data,
			dataType:'json',
			success:function(json){
				if(json == 1){
					$('#form_signup').submit();
					return;
				}

				// TODO: display failed messages in hilarious way
				for(var error in json.errors){
					console.log(json.errors[error][0]);
				}

			}
		});
	});

	$('#first_name').keydown(function(e){
		var key = e.which ? e.which : e.keycode
		if((key == 32 || key == 13) && this.value != ''){
			$('#last_name').focus();
			return false;
		}
	});

	$('#last_name').keydown(function(e){
		if((e.which ? e.which : e.keycode) == 8 && this.value == ''){
			$('#first_name').focus();
			return false;
		}
	});

	$('#last_name, #username, #email, #sex, #password, #password_confirmation').keydown(function(e){
		if((e.which ? e.which : e.keycode) == 13){
			step++;
			signUpNextPrev();
			return false;
		}
	});
});