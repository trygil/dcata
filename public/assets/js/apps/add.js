
	dcata.controller('AddController', function($scope, $http){
		$scope.girl = {
			status: 0,
			editable: 0
		};

		// Submit handler
		$scope.submitForm = function(){
			$http({
				url: 'add',
				method: 'POST',
				data: JSON.stringify($scope.girl)
			}).success(function(data){
				if(data.success != undefined){
					window.location.href = data.success.redirect;
				}
			});
		}
	});

	function uniqueField(e){
		// Check if exist
		var data = this.value.replace(/\s/gi,'').split(',');
		
		if($.inArray(e.attrs.value.replace(/\s/gi,''), data) > -1){
			return false;
		}
	}

$(document).ready(function(){
	$('.date').datetimepicker({pickTime: false});
	$('#callsign').tokenfield().on('tokenfield:createtoken', uniqueField);

	var tags = ['Hijaber','Aset!'];

	var bhound = new Bloodhound({
		datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
		queryTokenizer: Bloodhound.tokenizers.whitespace,
		local: $.map(tags, function(tag) { return { value: tag }; })
	});

	bhound.initialize();

	$('#tags').tokenfield({
  		typeahead: [null, { source: bhound.ttAdapter() }]
	}).on('tokenfield:createtoken', uniqueField);

	$('#btn_upload').click(function(){
		$('#photo').trigger('click');
	});

	$('#photo').change(function(){
		var file = this.files[0];
		var read = new FileReader();

		read.onload = function(e) {
			$('#photo_display').html('<div class="thumbnail"><img title="' + file.name + '" src="' + e.target.result + '"></div>');
			$('#btn_upload').html('Change Photo');
		};
		read.readAsDataURL(file);
	});

});