dcata.controller('DetailController', function($scope, $http){
	$scope.girl = {};

	var id = window.location.pathname.split('/')[2];
	
	$http.get('/view/data/' + id).then(function(response){
		$scope.girl = response.data;
		$scope.girl.photosCount = Object.keys($scope.girl.photos).length;
	});

	$scope.$on('ngRepeatFinished', function(e) {
    	var loaded = 0;
		$('.photos:eq(0) img').load(function(){
			++loaded;
			
			if(loaded == $scope.girl.photosCount){
				console.log($scope.girl.photosCount >= 4 ? 4: $scope.girl.photosCount)
				var msnry = new Masonry('.photos', {
					itemSelector: '.item',
					columnWidth: ($scope.girl.photosCount >= 4 ? 4: $scope.girl.photosCount),
					gutter: 4
				});

				msnry.on('layoutComplete', function(){
					console.log('Done');
				});
			}
		});
	});
});

$(document).ready(function(){
	var wrapper_height = $('#wrapper').height();
	var display_height = wrapper_height - $('#content').css('marginTop').replace('px', '');

	$('#display section:eq(0)').height(display_height);
	$('#display .main_view').css('margin-top', (display_height-$('#display h1.name').height()) / 2);
});