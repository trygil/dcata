<html>
	<head>
		<title>{{ $pageTitle }}</title>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
 
		<link rel="shortcut icon" type="image/ico" href="favicon.ico">
		<link rel="stylesheet" type="text/css" href="{{URL::to('assets/css/bootstrap.min.css')}}">
		<link rel="stylesheet" type="text/css" href="{{URL::to('assets/css/font-awesome.min.css')}}">
		<link rel="stylesheet" type="text/css" href="{{URL::to('assets/css/style.css')}}">
		<link rel="stylesheet" type="text/css" href="{{URL::to('assets/css/UI.css')}}">
		<link rel="stylesheet" type="text/css" href="{{URL::to('assets/css/part.css')}}">

		<script type="text/javascript" src="{{ URL::to('assets/js/jquery-2.1.1.min.js') }}"></script>
		<script type="text/javascript" src="https://code.angularjs.org/1.2.21/angular.min.js"></script>
		<script type="text/javascript" src="{{ URL::to('assets/js/bootstrap.min.js') }}"></script>
		<script type="text/javascript" src="{{ URL::to('assets/js/velocity.min.js') }}"></script>
		<script type="text/javascript" src="{{ URL::to('assets/js/velocity.ui.min.js') }}"></script>
		<script type="text/javascript" src="{{ URL::to('assets/js/main.js') }}"></script>
	</head>
	<body ng-app="dcata">
		@if(Auth::check())
		<nav class="nav navbar navbar-fixed-top" id="header">
			<div class="container">
				<div class="navbar-header">
					<a class="navbar-brand navbar-header logo" href="/">DCata</a>
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
				</div>

				@if(isset($header_title))
				<ul class="nav navbar-nav navbar-left">
					<li><span class="navbar-text header_title">{{$header_title}}</span></li>
				</ul>
				@endif
				
				<ul class="nav navbar-nav navbar-right collapse navbar-collapse navbar-ex1-collapse">
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">{{Auth::user()->first_name}} <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="{{URL::to('logout')}}">Logout</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</nav>
		@endif

		<div id="wrapper">
			@if(Session::has('success_message'))
				<div class="alert alert-success" role="alert">{{ Session::get('success_message') }}</div>
			@elseif(Session::has('error_message'))
				<div class="alert alert-danger" role="alert">{{ Session::get('error_message') }}</div>
			@endif

			{{ HTML::ul($errors->all()) }}

			@yield('content')
			<div class="spinner hide">
				<div class="double-bounce1"></div>
				<div class="double-bounce2"></div>
			</div>

			@if(Auth::check())
			<div id="footer">
				&copy; DCata {{ date('Y') }} &middot;
				<a href="{{URL::to('dcata')}}">About Us</a> &middot; 
				<a href="{{URL::to('developer')}}">Developer</a> &middot; 
				<a href="{{URL::to('help')}}">Help</a>
			</div>
			@endif
		</div>
	</body>
</html>