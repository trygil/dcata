@extends('base')

@section('content')
	<div id="sidebar" class="col-md-3">
		<ul class="nav nav-pills nav-stacked">
			<li><a href="{{ URL::to('admin') }}">Dashboard</a></li>
			<li><a href="admin/categories">Item Categories</a></li>
		</ul>
	</div>

	<div id="main_content" class="col-md-9">
		@if(Session::has('success_message'))
			<div class="alert alert-success" role="alert">{{ Session::get('admin_success_message') }}</div>
		@elseif(Session::has('error_message'))
			<div class="alert alert-danger" role="alert">{{ Session::get('admin_error_message') }}</div>
		@endif

		{{ HTML::ul($errors->all()) }}

		@yield('main_content')
		<script type="text/javascript" src="{{ URL::to('assets/js/main.js') }}"></script>
	</div>

@stop