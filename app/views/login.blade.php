@extends('base')

@section('content')
	<div id="main">
		<div id="login">
			<form method="post" action="{{URL::to('account/login')}}/">
				<div class="logo">DCata</div>
				<div class="main">
					<li><input type="text" name="username" placeholder="username" /></li>
					<li><input type="password" name="password" placeholder="password" /></li>
				</div>
				<div class="b">
					<li><button type="submit">Login</button></li>
					<li>
						<span>
						<!-- 	<a href="account/signup">Sign Up</a>
							&nbsp;&nbsp;&middot;&nbsp;&nbsp; -->
							<a href="#">Need help ?</a>
						</span>
					</li>
				</div>
				<input type="hidden" name="redirect" value="">
			</form>
		</div>
		<div style="position:absolute;bottom:1%;right:1%;font-size:110%">
			Don't have an account yet? <a href="account/signup" style="color:#06C;">Sign up</a>
		</div>
	</div>

@stop