@extends('home')

@section('maincontent')
	
	<link rel="stylesheet" type="text/css" href="{{URL::to('assets/css/apps/view.css')}}">
	<script type="text/javascript" src="{{ URL::to('assets/js/masonry.pkgd.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::to('assets/js/apps/view.js') }}"></script>
	
	<div class="col-md-12" id="display" ng-controller="DetailController">
		<section>
			<div class='main_view'>
				<h1 class="name">{{ $girl->name }}</h1>
				<p class="description">"&nbsp;{{ $girl->description }}&nbsp;"<p>
			</div>
		</section>

		<section>
			<div class="birthdate">
				<h2 class="classy">Birthdate:</h2>
				{{ $girl->birthdate . ' (<strong>' . $girl->age . ' years old</strong>)' }}
			</div>

			<div class="callsign">
				<h2 class="classy">Callsign:</h2>
				<ul>
					<li ng-repeat="(key, val) in girl.callsigns">[[val]]</li>
				</ul>
			</div>
		</section>
		<section>
			<div class="photos">
				<ul>
					<li class="item" ng-repeat="(key, val) in girl.photos" >
						<img 
						ng-src="/photos/[[girl.photosCount <= 2 ? '':'small/']][[val]]" 
						on-finish-render="imageLoaded">
					</li>
				</ul>
			</div>
		</section>
	</div>
	

@stop