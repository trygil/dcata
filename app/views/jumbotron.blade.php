@extends('base')

@section('content')
	
	<div id="main">
		@yield('main')
	</div>

	<div id="logo"><a href="{{URL::to('/')}}" class="logo">DCata</a></div>

@stop