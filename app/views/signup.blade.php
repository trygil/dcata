@extends('jumbotron')

@section('main')
	<script type="text/javascript" src="{{ URL::to('assets/js/apps/signup.js') }}"></script>
	{{ Form::open(['url' => 'account/signup', 'method' => 'post', 'id' => 'form_signup']) }}
	<div id="register" class="col-md-7" style="margin: auto; float:none;">
		<div class="show">
			<h3>What's your name Fred ?</h3>
			<div class="form-group">
				{{	Form::text('first_name', '', ['id' => 'first_name', 'class' => 'form-control inlineL', 'placeholder' => 'first name']) }}
				{{	Form::text('last_name', '', ['id' => 'last_name', 'class' => 'form-control inlineR', 'placeholder' => 'last name']) }}
			</div>
		</div>

		<div class="hide">
			<h3>Can i call you Foobla or what?</h3>
			<div class="form-group">
				{{	Form::text('username', '', ['id' => 'username', 'class' => 'form-control', 'placeholder' => 'username']) }}
			</div>
		</div>

		<div class="hide">
			<h3>I need your email for.. you know.. spamming your email</h3>
			<div class="form-group">
				{{	Form::text('email', '', ['id' => 'email', 'class' => 'form-control', 'placeholder' => 'email']) }}
			</div>
		</div>
		
		<div class="hide">
			<h3>Are you sure you're not a shemale?</h3>
			<div class="form-group">
				<label class="checkbox-inline">
					{{	Form::checkbox('sex', '1', false, ['id' => 'sex']) }} Male
				</label>
			</div>
		</div>

		<div class="hide">
			<h3>Password</h3>
			<div class="form-group">
				{{	Form::password('password', ['id' => 'password', 'class' => 'form-control', 'placeholder' => 'password']) }}
			</div>
		</div>

		<div class="hide">
			<h3>Can you re-enter your password? I forget to turn on the database</h3>
			<div class="form-group">
				{{	Form::password('password_confirmation', ['id' => 'password_confirmation', 'class' => 'form-control', 'placeholder' => 'confirmation password']) }}
			</div>
		</div>

		<div class="hide">
			<h3>Hhmmmm..</h3>
			<div class="form-group">
				<label for="terms" class="checkbox-inline">
					{{	Form::checkbox('terms', '1', false, ['id' => 'terms']) }}
					I accepted whatever <a href="#">you said</a>.
				</label>
			</div>
		</div>

		<hr>

		<div class="form-group right fixed bottom">
			<button class="btn btn-primary" id="next-btn" type="button">Next</button>
			<button class="btn" id="prev-btn" type="button">Previous</button>
			<button class="btn btn-primary" id="finish-btn" type="button">Finish!</button>
		</div>
	</div>
	{{ Form::close() }}
@stop