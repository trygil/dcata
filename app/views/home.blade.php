@extends('base')

@section('content')
	<script type="text/javascript" src="{{ URL::to('assets/js/apps/home.js') }}"></script>
	<div id="sidebar" class="col-md-2 col-sm-12">
		<ul>
			<li><a href="/"><span class="glyphicon glyphicon-home"></span> Home</a></li>
			<li><a href="/add"><span class="glyphicon glyphicon-plus"></span> Add Girl</a></li>
			<li><a href="/favorite"><span class="glyphicon glyphicon-heart"></span> Favorite</a></li>
		</ul>
	</div>

	@if(!isset($non_std_content))
	<div id="content" class="col-md-{{isset($content_width) ? $content_width : '11'}} col-sm-12">
	@endif
		
		@yield('maincontent')
	
	@if(!isset($non_std_content))
	</div>
	@endif
@stop