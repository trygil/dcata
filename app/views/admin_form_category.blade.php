@extends('dashboard')

@section('main_content')

	<h3>{{ $action_name }} Ketegori</h3>

	<?php
		foreach($parents as $key => $val){
			echo $key . ' ' . $val;
		}
	?>

	{{ Form::open(['url' => 'admin/categories/add', 'method' => 'post', 'id' => 'form_kategori', 'class' => 'form-horizontal']) }}
		<div class="form-group">
			{{	Form::label('category', 'Kategori', ['class' => 'left']),
				Form::text('category', '', ['id' => 'category', 'class' => 'form-control', 'placeholder' => 'kategori']) }}
		</div>
		<div class="form-group">
			{{ 	Form::label('childof', 'Sub Kategori dari', ['class' => 'left']),
				Form::select('childof', $parents, 0, ['id' => 'childof', 'class' => 'form-control']) }}
		</div>
		<div class="form-group">
			<button class="btn btn-primary" type="submit">Save</button>
			<button class="btn" type="button">Batal</button>
		</div>
	{{ Form::close() }}

@stop