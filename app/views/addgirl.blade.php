@extends('home')

@section('maincontent')
	<link rel="stylesheet" type="text/css" href="{{URL::to('assets/css/bootstrap-datetimepicker.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{URL::to('assets/css/tokenfield-typeahead.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{URL::to('assets/css/bootstrap-tokenfield.min.css')}}">

	<script type="text/javascript" src="{{ URL::to('assets/js/moment.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::to('assets/js/bootstrap-datetimepicker.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::to('assets/js/typeahead.bundle.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::to('assets/js/bootstrap-tokenfield.min.js') }}"></script>
	
	<script type="text/javascript" src="{{ URL::to('assets/js/apps/add.js') }}"></script>
	<div class="col-md-12 form-wrap" style="margin: auto; float:none;" ng-controller="AddController">
	<form name="gurlform" id="form_signup" novalidate ng-submit="submitForm()">
		<div class="form-group col-md-6">
			<h3><label for="name">Name</label></h3>
			{{ Form::text('name', '', ['id' => 'name', 'class' => 'form-control col-md-12', 'placeholder' => 'full name','ng-model'=>'girl.full_name']) }}
		</div>

		<div class="form-group col-md-6">
			<h3><label for="birthdate">Birthdate</label></h3>
			{{	Form::text('birthdate', '', ['id' => 'birthdate', 'data-format' => 'DD/MM/YYYY', 'class' => 'date form-control', 'placeholder' => 'birthdate (dd/mm/yyyy)','ng-model'=>'girl.birthdate', 'autocomplete' => 'off']) }}
			
		</div>

		<div class="form-group col-md-7">
			<h3><label for="single">Recent Relationship Status</label></h3>
			{{	Form::select('single', ['0' => 'Single', '1' => 'In Relationship', '3' => 'Unknown'], '0', ['class'=>'form-control', 'id'=>'single','ng-model'=>'girl.status']) }}

			<h3><label for="editby">Editable By</label></h3>
			{{	Form::select('editby', ['0' => 'Everybody', '1' => 'Only Me'], '0', ['class'=>'form-control', 'id'=>'editby','ng-model'=>'girl.editable']) }}
		</div>

		<div class="form-group col-md-5">
			<h3><label for="address">Address</label></h3>
			{{	Form::textarea('address', '', ['class'=>'form-control', 'id'=>'address', 'placeholder'=>'address..', 'rows'=>5,'ng-model'=>'girl.address']) }}
		</div>
		
		<div class="form-group col-md-12">
			<h3><label for="description">Description</label></h3>
			{{	Form::textarea('description', '', ['class'=>'form-control', 'id'=>'description', 'placeholder'=>'lovely description..', 'rows'=>4,'ng-model'=>'girl.description']) }}
		</div>

		<div class="form-group col-md-12">
			<h3><label for="callsign">Callsign(s)</label></h3>
			{{	Form::text('callsign', '', ['id' => 'callsign', 'class' => 'form-control', 'placeholder' => "eg: 'aloha' 'woopwoop' 'scoopia'",'ng-model'=>'girl.callsign']) }}
		</div>

		<div class="form-group col-md-12">
			<h3><label for="tags">*Tag(s)</label></h3>
			{{	Form::text('tags', '', ['id' => 'tags', 'class' => 'form-control', 'placeholder' => 'tags..','ng-model'=>'girl.tags']) }}
		</div>

		<div class="form-group col-md-12">
			<hr>
			<div id="photo_display"></div>
			<button id="btn_upload" type="button" class="btn">Choose Photo</button>
			{{	Form::file('photo', ['id' => 'photo', 'class' => 'hide','fileread' => 'girl.photo','ng-model' => 'girl.photo']) }}
			<hr>
		</div>

		<div class="form-group col-md-12">
			<h3><label for="facebook">More Stalking</label></h3>
		</div>	
		<div class="form-group col-md-6">
			<div class="input-group">
				<div class="input-group-addon" title="facebook"><span class="fa fa-facebook"></span></div>
				{{	Form::text('facebook', '', ['id' => 'facebook', 'class' => 'form-control', 'placeholder' => 'facebook account','ng-model'=>'girl.social.facebook']) }}
			</div>
		</div>
		<div class="form-group col-md-6">
			<div class="input-group">
				<div class="input-group-addon" title="twitter"><span class="fa fa-twitter"></span></div>
				{{	Form::text('twitter', '', ['id' => 'twitter', 'class' => 'form-control', 'placeholder' => 'twitter account','ng-model'=>'girl.social.twitter']) }}
			</div>
		</div>
		<div class="form-group col-md-6">
			<div class="input-group">
				<div class="input-group-addon" title="instagram"><span class="fa fa-instagram"></span></div>
				{{	Form::text('instagram', '', ['id' => 'instagram', 'class' => 'form-control', 'placeholder' => 'instagram account','ng-model'=>'girl.social.instagram']) }}
			</div>
		</div>
		<div class="form-group col-md-6">
			<div class="input-group">
				<div class="input-group-addon" title="g+"><span class="fa fa-google-plus"></span></div>
				{{	Form::text('google_plus', '', ['id' => 'google_plus', 'class' => 'form-control', 'placeholder' => 'g+ account','ng-model'=>'girl.social.google_plus']) }}
			</div>
		</div>

		<div class="form-group right fixed bottom">
			<hr>
			<button class="btn" type="submit">Submit</button>
		</div>
	{{ Form::close() }}
	</div>
@stop