<?php

	# Class DCommon filled by re-usable magic functions
 	
 	namespace DCata; # Ini namespace, n-a-m-e-s-p-a-c-e nemspes!
 	use \DateTime;
 	use \DateTimeInterval;

	class DCommon{

		static $ext_mime = array(
			"pdf"=>"application/pdf",
			"exe"=>"application/octet-stream",
			"zip"=>"application/zip",
			"docx"=>"application/msword",
			"doc"=>"application/msword",
			"xls"=>"application/vnd.ms-excel",
			"ppt"=>"application/vnd.ms-powerpoint",
			"gif"=>"image/gif",
			"png"=>"image/png",
			"jpeg"=>"image/jpg",
			"jpg"=>"image/jpg",
			"mp3"=>"audio/mpeg",
			"wav"=>"audio/x-wav",
			"mpeg"=>"video/mpeg",
			"mpg"=>"video/mpeg",
			"mpe"=>"video/mpeg",
			"mov"=>"video/quicktime",
			"avi"=>"video/x-msvideo",
			"3gp"=>"video/3gpp",
			"css"=>"text/css",
			"jsc"=>"application/javascript",
			"js"=>"application/javascript",
			"php"=>"text/html",
			"htm"=>"text/html",
			"html"=>"text/html"
		);

		static $mime_ext = array(
			"application/pdf"=>"pdf",
			"application/octet-stream"=>"exe",
			"application/zip"=>"zip",
			"application/msword"=>"docx",
			"application/msword"=>"doc",
			"application/vnd.ms-excel"=>"xls",
			"application/vnd.ms-powerpoint"=>"ppt",
			"image/gif"=>"gif",
			"image/png"=>"png",
			"image/jpeg"=>"jpg",
			"image/jpg"=>"jpg",
			"audio/mpeg"=>"mp3",
			"audio/x-wav"=>"wav",
			"video/mpeg"=>"mpeg",
			"video/mpeg"=>"mpg",
			"video/mpeg"=>"mpe",
			"video/quicktime"=>"mov",
			"video/x-msvideo"=>"avi",
			"video/3gpp"=>"3gp",
			"text/css"=>"css",
			"application/javascript"=>"jsc",
			"application/javascript"=>"js",
			"text/html"=>"php",
			"text/html"=>"htm",
			"text/html"=>"html"
		);

		public static function changeDateFormat($date, $from_format, $to_format){
			return date_format(date_create_from_format($from_format, $date), $to_format);
		}

		public static function age($date, $from_format, $format){
			$now = new DateTime();
			$birthdate = date_create_from_format($from_format, $date);

			return $now->diff($birthdate)->format($format);
		}

		public static function isImage($arg){
			if(isset(self::$ext_mime[$arg])){
				if(strpos(self::$ext_mime[$arg], 'image') !== false)
					return true;
			}

			if(isset(self::$mime_ext[$arg])){
				if(strpos($arg, 'image') !== false)
					return true;
			}

			return false;
		}

		public static function extensionFromMime($mime){
			return isset(self::$mime_ext[$mime])  ? self::$mime_ext[$mime] : false;
		}



		public static function mimeFromExtension($ext){
			return isset(self::$ext_mime[$ext])  ? self::$ext_mime[$ext] : false;
		}
	};
