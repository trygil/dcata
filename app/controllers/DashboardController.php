<?php

class DashboardController extends BaseController {

	public function dashboard(){
		return View::make('dashboard');
	}

	public function listCategories(){
		$parents = Category::all();
		return View::make('admin_categories', ['pageTitle' => 'Categories', 'parents'=>$parents]);
	}

	public function addCategoryForm(){
		$parents = [0=>'---'];
		
		foreach(Category::all() as $cat){
			$parents[$cat->id] = $cat->category;			
		}
		return View::make('admin_form_category', ['pageTitle' => 'Add Category', 'action_name'=>'Tambah', 'parents'=>$parents]);	
	}

	public function editCategoryForm($id = NULL){
		$parents = [0=>'---'];

		foreach(Category::all() as $cat){
			$parents[$cat->id] = $cat->category;			
		}
		return View::make('admin_form_category', ['pageTitle' => 'Edit Category', 'action_name'=>'Edit', 'parents'=>$parents]);
	}

	public function addCategory(){
		return $this->saveCategory();
	}
	
	public function editCategory($id = NULL){
		if($id != NULL)
			return $this->saveCategory($id);
		else
			;

	}

	private function saveCategory($id = NULL){
		$rules = array(
			'category' => 'required',
			'childof' => 'required'
		);

		$validator = Validator::make(Input::all(), $rules);

		$data = array(
			'category' => Input::get('category'),
			'childof' => Input::get('childof')
		);

		if($validator->fails()){
			return Redirect::to('admin/categories')
					->withErrors($validator)
					->withInput();
		}
		else{
			if($id == NULL) {
				# Insert
				$category = new Category();
				$category->category = $data['category'];
				$category->parent = $data['childof'];

				Session::flash('admin_success_message', 'Sukses menambah kategori!');
				$category->save();
			}
			else{
				# Update
			}

			return Redirect::to('admin/categories');

		}
	}
};