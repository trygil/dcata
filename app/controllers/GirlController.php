<?php

use DCata\DCommon;

class GirlController extends BaseController {

	# Submit Handler
	public function add(){

		if(Request::wantsJson()){

			$rules = [
				'full_name' => 'required',
				'birthdate' => 'required|date_format:d/m/Y',
				'status' => 'required',
				'editable' => 'required',
				'address' => 'required',
				'description' => 'required',
				'callsign' => 'required',
				'tags' => 'required',
				'photo' => 'required'
			];

			$messages = [];

			$validator = Validator::make(Input::all(), $rules);

			if($validator->fails()){
				$messages['error']['messages'] = $validator->messages();
				return Response::json($messages);
			}

			$tmp = explode(';', str_replace('data:', '', Input::get('photo')));
			$mime = '';
			if(is_array($tmp) && count($tmp) > 0)
				$mime = $tmp[0];

			if(!DCommon::isImage($mime)){
				$messages['error']['messages'] = ['photo'=>'Not valid photo file'];
				return Response::json($messages);
			}


			# Transaction
			// DB::transaction(function(){

				$data = Input::all();

				$tmp = explode(';', str_replace('data:', '', $data['photo']));
				$mime = '';
				if(is_array($tmp) && count($tmp) > 0)
					$mime = $tmp[0];

				# Save Data
				$girl = new Girl();
				$girl->name 		= $data['full_name'];
				$girl->birthdate 	= DCommon::changeDateFormat($data['birthdate'], 'd/m/Y', 'Y-m-d');
				$girl->address 		= $data['address'];
				$girl->description 	= $data['description'];
				$girl->single 		= $data['status'];
				$girl->editable 	= $data['editable'];
				$girl->addby 		= Auth::user()->id;
					
				// Save 'subject' first
				$girl->save();

				# Callsign Block
				$callsigns = [];
				foreach (explode(',', Input::get('callsign')) as $val) {
					$callsign = new Callsign;
					$callsign->code = $val;
					$callsigns[] = $callsign;
				}

				$girl->callsigns()->saveMany($callsigns);

				# Tags
				$tags = [];
				foreach (explode(',', Input::get('tags')) as $val) {
					$tag = Tag::where('tag', '=', $val);

					if($tag->count())
						$tag = $tag->first();
					else
						$tag = new Tag;
					
					$tag->tag = $val;
					$tags[] = $tag;
				}

				$girl->tags()->saveMany($tags);


				# Stalking Part
				$social = Input::get('social');
				if(is_array($social)){
					foreach ($social as $type => $value) {
						
						$contact = Contact::where('type', '=', strtolower($type));

						if($contact->count())
							$contact = $contact->first();
						else{
							$contact = new Contact;
							$contact->type = $type;
							$contact->save();
						}

						foreach (explode(',', $value) as $val) {
							if($contact->is_url){
								if(filter_var($val, FILTER_VALIDATE_URL))
									$girl->contacts()->attach($contact->id , ['value' => $val]);
							}
							else
								$girl->contacts()->attach($contact->id , ['value' => $val]);

						}
					}
				}

				# Photo Part
				$file_name = md5(date('YmdHis') . $girl->id);
				$file_ext = DCommon::extensionFromMime($mime);

				$photo = new Photo;
				$photo->name = $file_name . '.' . $file_ext;

				$girl->photos()->save($photo);

				// Updating 'profile_picture' field
				$girl->profile_picture = $photo->id;
				$girl->save();
							

				// Save photo to directory
				$photos_path = public_path() . '/photos/';
				$small_path  = $photos_path . '/small/';
				$nano_path 	 = $photos_path . '/nano/';

				$image = Image::make(Input::get('photo'));

				// Original Photo
				if($image->width() > 1500)
					$image->widen(1024);

				$image->save($photos_path . $file_name . '.' . $file_ext);
				
				// Resize Photo
				if($image->width() > 300)
					$image->widen(300);

				$image->save($small_path . $file_name . '.' . $file_ext);

				if($image->width() > 150)
					$image->widen(150);
				
				$image->save($nano_path . $file_name . '.' . $file_ext);
			
			// });

			$messages = ['success' => [
							'messages' => ['New girl! All hail '.Auth::user()->name.'!'],
							'redirect' => URL::to('/view/'.$girl->id)
						]];

			return Response::json($messages);
		}

		return App::abort(404);
	}

	# Display
	public function view($id){
		$data = Girl::find($id);
		
		$data->age = DCommon::age($data->birthdate, 'Y-m-d', '%Y');
		$data->birthdate = DCommon::changeDateFormat($data->birthdate, 'Y-m-d', 'd/M/Y');

		return View::make('girl_view', ['pageTitle'=>$data->name, 'girl'=>$data, 'header_title'=>$data->name,'content_width'=>12]);
	}

	public function data($id = null){
		
		if(Request::ajax() || Request::wantsJson()){
			if($id != null){
				$girl = [];

				$data = Girl::find($id);
				
				$photos = $data->photos;
				$callsigns = $data->callsigns;
				$contacts = $data->contacts;
				$tags = $data->tags;

				foreach ($data->toArray() as $key => $value) {
					$girl[$key] = $value;
				}

				$girl['photos'] = [];
				foreach ($photos as $value) {
					$girl['photos'][$value['id']] = $value['name'];
				}

				$girl['callsigns'] = [];
				foreach ($callsigns as $value) {
					$girl['callsigns'][$value['id']] = $value['code'];
				}

				$girl['contacts'] = [];
				foreach ($contacts as $value) {
					$girl['contacts'][$value['id']] = $value['pivot']['value'];
				}

				$girl['tags'] = [];
				foreach ($tags as $value) {
					$girl['tags'][$value['id']] = $value['tag'];
				}

				
				return Response::json($girl);

			}
		}

		return Response::json(["AAAA"]);//App::abort(404);
	}
};