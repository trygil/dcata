<?php

class AccountController extends BaseController {

	public function signup(){

		$rules = array(
			'first_name' => 'required',
			'last_name' => 'required',
			'username' => 'required|alpha_dash|min:5|max:20|unique:user',
			'email' => 'required|email|unique:user',
			'sex' => 'required|accepted',
			'password' => 'required|min:8|confirmed',
			'terms' => 'required|accepted'
		);

		$validator = Validator::make(Input::all(), $rules);

		$data = array(
			'first_name' => Input::get('first_name'),
			'last_name' => Input::get('last_name'),
			'username' => Input::get('username'),
			'email' => Input::get('email'),
			'sex' => Input::get('sex'),
			'password' => Hash::make(Input::get('password'))
		);

		if($validator->fails()){
			$errors = $validator->errors();
			$failed = $validator->failed();
			return Response::json(['errors' => $errors, 'failed' => $failed]);
		}
		
		if(!Request::ajax()){

			$user = new User();
			$user->first_name = $data['first_name'];
			$user->last_name = $data['last_name'];
			$user->username = $data['username'];
			$user->email = $data['email'];
			$user->password = $data['password'];
			$user->user_group = $data['user_group'];

			Session::flash('success_message', 'Sign Up successfully!');
			$user->save();
			
			return Redirect::to('/');
		}
		else{
			return "1";
		}

	}
};