<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function index(){
		if(Request::ajax())
			return Response::json($this->getDataList());
		else
			return View::make('listgurl', ['pageTitle'=>'DCata', 'content_width'=>12]);
	}

	public function login(){
		$rules = array(
			'username' => 'required',
			'password' => 'required'
		);

		$validator = Validator::make(Input::all(), $rules);

		if($validator->fails()){
			// TODO : return error
			return Redirect::to('/');
		}
		else{

			$data = array(
				'username' => Input::get('username'),
				'password' => Input::get('password')
			);

			if(Auth::attempt($data))
				return Redirect::to('/');
			
			else{
				// TODO : return error
				return Redirect::to('/');
			}

		}

	}

	public function logout(){
		Auth::logout();
		return Redirect::to('/');
	}

	public function admin(){
		return 'Admin';
	}

	private function getDataList(){
		$data = [];

		foreach (Girl::all() as $girl) {
			$photo = $girl->photos()->where('id','=',$girl->profile_picture)->first()->name;
			$girl->photo = $photo;
			$data[] = $girl;
		}
		
		return $data;
	}
}