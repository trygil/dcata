<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::post('account/login', "HomeController@login");
Route::get('account/signup', function(){
	if(Auth::check())
		return App::abort(404);

	return View::make('signup', ['pageTitle' => 'Sign Up']);
});
Route::post('account/signup', 'AccountController@signup');

Route::group(array('before' => 'auth'), function(){

	Route::get('/', "HomeController@index");
	Route::get('list', "HomeController@index");
	Route::get('logout', "HomeController@logout");

	# Girls
	Route::get('add', function(){
		return View::make('addgirl', ['pageTitle'=>'Add Gurl', 'content_width' => '7', 'header_title'=>'Add Girl']);
	});
	Route::post('add', "GirlController@add");
	Route::get('view/{id}', "GirlController@view");
	Route::get('view/data/{id?}', "GirlController@data");

	# Account
	Route::group(array('prefix' => 'account'), function(){
	});

	# Admin
	Route::group(array('prefix' => 'overlord', 'before' => 'admin'), function(){
		Route::get('/', 'DashboardController@dashboard');
	});

});