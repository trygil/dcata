<?php

class Callsign extends Eloquent{	
	protected $table = 'callsign';

	public function girls(){
		return $this->belongsToMany('Girl', 'subject_callsign', 'callsign_id', 'subject_id');
	}
}