<?php

class Girl extends Eloquent{	
	protected $table = 'subject';

	public function photos(){
		return $this->hasMany('Photo', 'subject_id', 'id');
	}

	public function callsigns(){
		return $this->belongsToMany('Callsign', 'subject_callsign', 'subject_id', 'callsign_id');
	}

	public function tags(){
		return $this->belongsToMany('Tag', 'subject_tag', 'subject_id', 'tag_id');
	}

	public function contacts(){
		return $this->belongsToMany('Contact', 'subject_contact', 'subject_id', 'contact_id')->withPivot('value');
	}
}