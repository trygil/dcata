<?php

class Photo extends Eloquent{	
	protected $table = 'photo';

	public function girls(){
		return $this->belongsTo('Girl', 'id', 'subject_id');
	}
}