<?php

class Tag extends Eloquent{	
	protected $table = 'tag';

	public function girls(){
		return $this->belongsToMany('Girl', 'subject_tag', 'tag_id', 'subject_id');
	}
}