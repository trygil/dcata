<?php

class Contact extends Eloquent{	
	protected $table = 'contact';

	public function girls(){
		return $this->belongsToMany('Girl', 'subject_tag', 'contact_id', 'subject_id')->withPivot('value');
	}
}